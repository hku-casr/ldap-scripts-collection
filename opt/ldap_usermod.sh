#!/bin/bash

#
# Name: ldap_usermod.sh
#
# Description: This script modify a user record in LDAP server. The script will
#  check against the LDAP server for incorrect paramenters and abort.
#
# The script will detect if a variable ldappw is defined and leverage directly
#  or will prompt for LDAP root DN password if the variable was not defined.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common.sh;

usage="Modify User Account Attribute on the LDAP Server\n
Usage: $0 <username> {add | modify | delete} <attribute> [predicate] [new_value]\n
 e.g.: $0 abaker modify loginShell \"/bin/csh\"\n
  or   $0 abaker modify host americano00 americano01\n
  or   $0 abaker add host bluemountain\n
  or   $0 abaker delete manager\n
  or   $0 abaker delete host bluemountain";

# PARSE COMPULSORY PARAMETERS
#
if [ $# -ge 3 ]; then
    usrid=$1;

    case $2 in
        add) act="add";
            if [ $# -eq 4 ]; then
                new_val=$4;
            else
                echo -e $usage;

                exit 1;
            fi; ;;
        modify)
            if [ $# -eq 4 ]; then
                act="replace";

                new_val=$4;
            elif [ $# -eq 5 ]; then
                act="modify"

                predicate=$4;
                new_val=$5;
            else
                echo -e $usage;

                exit 1;
            fi; ;;
        delete) act="delete"
            if [ $# -eq 3 ]; then
                predicate="";
            elif [ $# -eq 4 ]; then
                predicate=$4;
            else
                echo -e $usage;

                exit 1;
            fi; ;;
    esac;

    attribute=$3;
else echo -e $usage;

    exit 1;
fi;

#
# STAGE 2
# #######
#
# LDAP DATA QUERY AND PRE-CONDITION CHECK
#

# CHECK FOR USER EXISTENCE.
#
if [ `ldapsearch -x -LLL -H $ldapsrv_prot://$ldapsrv -b "ou=$user_ou,$base_dn" "(uid=$usrid)" 1.1 | wc -l` -eq 0 ]; then
    echo "FAILED: User $usrid does not exist";

    exit 1;
fi;

# CHECK FOR ATTRIBUTE EXISTENCE AND PREDICATE REQUIREMENT
#
attr_cnt=$(ldapsearch -x -LLL -H $ldapsrv_prot://$ldapsrv -b "ou=$user_ou,$base_dn" | grep "$attribute" | wc -l);

if [ $act == "delete" ] || [ $act == "replace" ] || [ $act == "modify" ]; then
    if [ $attr_cnt -eq 0 ]; then
        echo "FAILED: Attribute $attribute does not exist";

        exit 1;
    fi;
fi;

if [ $act == "delete" ] || [ $act == "replace" ]; then
    if [ ! $attr_cnt -eq 1 ] && [ -z "$predicate" ]; then
        echo "FAILED: Attribute is not singular, predicate required";

        exit 1;
    fi;
fi;

#
# STAGE 3
# #######
#
# LDAP ENTRY CONSTRUCTION AND COMMIT
#

# CONSTRUCT LDAP DATA INTERCHANGE FORMAT ENTRY
#
ldif="dn: uid=$usrid,ou=$user_ou,$base_dn
changetype: modify"

if [ $act == "add" ] || [ $act == "replace" ] ; then
    ldif="$ldif
$act: $attribute
$attribute: $new_val";
elif [ $act == "delete" ]; then
    ldif="$ldif
$act: $attribute";

    if [ ! -z "$predicate" ]; then
        ldif="$ldif
$attribute: $predicate";
    fi;
elif [ $act == "modify" ]; then
    ldif="$ldif
delete: $attribute
$attribute: $predicate
-
add: $attribute
$attribute: $new_val";
fi;

if [ -z ${ldappw+x} ]; then
    ldapmodify -H $ldapsrv_prot://$ldapsrv -D "cn=$root_cn,$base_dn" -W <<!
$ldif
!
else
    ldapmodify -H $ldapsrv_prot://$ldapsrv -D "cn=$root_cn,$base_dn" -w $ldappw <<!
$ldif
!
fi;

echo "SUCCESS: Modified user $usrid";
