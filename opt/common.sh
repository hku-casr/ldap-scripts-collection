#!/bin/bash

#
# Name: common.sh
#
# Description: This script defines common variables and establish common
#  environments for scripts under the LDAP sysadmin category.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

slaputilsrv="bluemountain.eee.hku.hk";
ldapsrv="bluemountain.eee.hku.hk";
ldapsrv_prot="ldaps";
base_dn="dc=casr,dc=eee,dc=hku,dc=hk";
user_ou="People";
group_ou="Groups";
root_cn="admin";
