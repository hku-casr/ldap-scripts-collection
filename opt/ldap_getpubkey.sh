#!/bin/bash

#
# Name: ldap_getpubkey.sh
#
# Description: This script fetches public keys of the user named if the user
#  has been authorised to use the script-running system.
#
# In order to use the script, administrator may fill the "Host Adaptation
#  Declaration" area with "clus" and/or "host" variable for adaptation. The
#  "clus" variable is to check whether a user has been authorised to access a
#  specific group of systems as a shorthand, especially, to save the need to
#  specify each individual nodes of a cluster. The "host" variable is to check
#  whether the user has been authorised to access the named host. The variable
#  will be propogated with the local hostname obtained from the environment.

source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common.sh;

# HOST ADAPTATION DECLARATION
#

# clus="americano";
# host="americano00";

#
# END OF HOST ADAPTATION DECLARATION

usage="Fetch Public Key from LDAP Server\n
Usage: $0 <username>\n
 e.g.: $0 abaker";

if [ $# -ne 1 ]; then
    echo -e $usage;

    exit 1;
fi;

if [ ! -z "$clus" ]; then
    realm_pred="(realm=$clus)";
elif [ ! -z "$CLUSTERNAME" ]; then
    realm_pred="(realm=$CLUSTERNAME)";
fi;

if [ ! -z "$host" ]; then
    host_pred="(host=$host)";
else
    host_pred="(host=`echo $HOSTNAME | cut -d "." -f1`)";
fi;

if [ ! -z "$realm_pred" ] && [ ! -z "$host_pred" ]; then
    predicate="(|$realm_pred$host_pred)";
else
    predicate="$realm_pred$host_pred";
fi;

if [ ! -z "$DEBUG" ] && [ "$DEBUG" == "yes" ]; then
    echo -e "[`date`] GET $1 FROM $ldapsrv_prot://$ldapsrv\n  PREDICATE $predicate" >> /tmp/ldap_getpubkey.log;
fi;

ldapsearch -x -H $ldapsrv_prot://$ldapsrv -b "$base_dn" -o ldif-wrap=no '(&(objectClass=posixAccount)'$predicate'(uid='$1'))' | grep "sshPublicKey:" | tr -s " " | cut --complement -d " " -f 1;
