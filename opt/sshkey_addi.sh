#!/bin/bash

#
# Name: sshkey_addi.sh
#
# Description: This is an interactive script that Adds a SSH Public Key to the
#  LDAP entry of the authenticated user. The script is expected to be ran
#  behind xinetd telnet service or as login shell of dummy user for SSH login.
#  The service should be protected to allow limited access to trusted hosts.
#  Registered key may be used for authentication if the system supports SSH
#  Public Key authentication against a LDAP server.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common.sh;

tty > /dev/null;

# FOR TELNET-NATURED ACCESSES THAT DO NOT HAVE PTY ALLOCATED.
#
if [ $? -ne 0 ]; then
    # FOR STUNNEL FORWARDED ACCESSES, OPENSSL CLIENT CANNOT HANDLE TELNET
    #  PROTOCOL COMMAND TO MASK PASSWORD INPUT LOCALLY. THEREFORE, ANSI ESCAPE
    #  CODE HAS TO BE USED INSTEAD.
    #
    if [ ${REMOTE_HOST} == "127.0.0.1" ]; then
        echo -ne "Username: ";
        read usrid;

        echo -ne "Password: ";
        read passwd;
        echo -ne "\x1B[1F\x1B[0K\r";
        echo -ne "Password: ";

        echo -ne "\r\n";
        echo -ne "SSH Public Key: ";
        read -r pubkey;

    # FOR REAL TELNET ACCESSES THAT CAN HANDLE TELNET PROTOCOL COMMAND TO HIDE
    #  PASSWORD, USE TELNET "INTERPRET AS COMMAND" BYTE ESCAPED TELNET COMMAND
    #  DIRECTLY.
    #
    else
        echo -ne "Username: ";
        read usrid;

        echo -ne "\xFF\xFB\x01";
        echo -ne "Password: ";
        read passwd;

        echo -ne "\r\n";
        echo -ne "\xFF\xFC\x01";
        echo -ne "SSH Public Key: ";
        read -r pubkey;
    fi;

    usrid=$(echo $usrid | tr -dc "[[:print:]]");
    passwd=$(echo $passwd | tr -dc "[[:print:]]");
    pubkey=$(echo $pubkey | tr -dc "[[:print:]]");

# FOR SSH ACCESSES THAT COME WITH PTY ALLOCATED, SILENT MODE IN READ COMMAND
#  IS EFFECTIVE AS TERMINAL EXISTS.
#
else
    clear;
    clear;

    read -p "Username: " usrid;
    read -sp "Password: " passwd;
    echo "";
    read -rp "SSH Public Key: " pubkey;
fi;

ssh-keygen -B -f /dev/stdin <<< "$pubkey" > /dev/null;

if [ $? -eq 0 ]; then
    ldif="dn: uid=$usrid,ou=$user_ou,$base_dn
changetype: modify
add: sshPublicKey
sshPublicKey: $pubkey";

    ldapmodify -H $ldapsrv_prot://$ldapsrv -D "uid=$usrid,ou=$user_ou,$base_dn" -w $passwd <<!
$ldif
!
fi;
