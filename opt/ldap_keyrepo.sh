#!/bin/bash

#
# Name: ldap_keyrepo.sh
#
# Description: This script clones public keys of users authorised to use the
#  script-running system into a centralised key repository. The repository
#  location is specified by the authorized_keys keyword configured in
#  sshd_config(5) file.
#
# In order to use the script, administrator may fill the "Host Adaptation
#  Declaration" area with "clus" and/or "host" variable for adaptation. The
#  "clus" variable is to check whether a user has been authorised to access a
#  specific group of systems as a shorthand, especially, to save the need to
#  specify each individual nodes of a cluster. The "host" variable is to check
#  whether the user has been authorised to access the named host. The variable
#  will be propogated with the local hostname obtained from the environment.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common.sh;

# HOST ADAPTATION DECLARATION
#

# clus="americano";
# host="americano00";
#
# PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"; # REQUIRED, WHEN TO BE RAN AS CRON JOB

#
# END OF HOST ADAPTATION DECLARATION

if [ $? -ne 0 ]; then
    echo "FAIL: Acquire Key Repository Location";

    exit 1;
fi;

if [ ! -z "$clus" ]; then
    realm_pred="(realm=$clus)";
elif [ ! -z "$CLUSTERNAME" ]; then
    realm_pred="(realm=$CLUSTERNAME)";
fi;

if [ ! -z "$host" ]; then
    host_pred="(host=$host)";
else
    host_pred="(host=`echo $HOSTNAME | cut -d "." -f1`)";
    host=`echo $HOSTNAME | cut -d "." -f1`;
fi;

source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common_host.sh;

if [ ! -z "$realm_pred" ] && [ ! -z "$host_pred" ]; then
    predicate="(|$realm_pred$host_pred)";
else
    predicate="$realm_pred$host_pred";
fi;

for i in `ldapsearch -x -H $ldapsrv_prot://$ldapsrv -b "$base_dn" "(&(objectClass=posixAccount)$predicate)" | grep "^uid:" | cut -d " " -f2`; do
    afile=`mktemp`;

    clus=$clus realm=$realm DEBUG="no" $auth_proj/ldap_getpubkey $i > $afile;

    diff $afile $key_repo/$i > /dev/null;

    if [ $? -ne 0 ]; then
        echo "INFO: Changes Found. Modify Key File for LDAP User $i";

        cat $afile > $key_repo/$i;

        chmod 644 $key_repo/$i;
    fi;

    rm -f $afile;
done;
