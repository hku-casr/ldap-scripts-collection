#!/bin/bash

#
# Name: local_keyrepo.sh
#
# Description: This script clones public keys of local users declared in their
#  authorized_keys files into a centralised key repository. The repository
#  location is specified by the authorized_keys keyword configured in
#  sshd_config(5) file. Both authorized_keys and authorized_keys2 files will be
#  cloned and concatenated into a single file.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common.sh;

# HOST ADAPTATION DECLARATION
#

# PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"; # REQUIRED, WHEN TO BE RAN AS CRON JOB

#
# END OF HOST ADAPTATION DECLARATION

if [ -z "$host" ]; then
    host=`echo $HOSTNAME | cut -d "." -f1`;
fi;

source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common_host.sh;

if [ $? -ne 0 ]; then
    echo "FAIL: Acquire Key Repository Location";

    exit 1;
fi;

ldapusr=`ldapsearch -x -H $ldapsrv_prot://$ldapsrv -b "$base_dn" "(objectClass=posixAccount)" | grep "^uid:" | cut -d " " -f2`;

while read line; do
    home=`echo $line | cut -d ":" -f6`;
    uid=`echo $line | cut -d ":" -f1`;

    found=0;

    for i in $ldapusr; do
        if [ $i == $uid ]; then
            found=1;

            break;
        fi;
    done;

    if [ $found -eq 1 ]; then
        continue;
    fi;

    if [ -e $home/.ssh ]; then
        afile=`mktemp`;

        cat $home/.ssh/authorized_keys $home/.ssh/authorized_keys2 > $afile;

        diff $afile $key_repo/$uid > /dev/null;

        if [ $? -ne 0 ]; then
            echo "INFO: Changes Found. Modify Key File for Local User $i";

            cat $afile > $key_repo/$uid;

            chmod 644 $key_repo/$uid;
        fi;

        rm -f $afile;
    fi;
done < <(getent passwd);