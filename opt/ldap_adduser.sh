#!/bin/bash

#
# Name: ldap_adduser.sh
#
# Description: This script adds a new user to the LDAP server. The script will
#  generate a randomised 8 character password for the user during the process.
#  The script will check against the LDAP server for possible duplicate entries
#  or incorrect paramenters and abort.
#
# The script will detect if a variable ldappw is defined and leverage directly
#  or will prompt for LDAP root DN password if the variable was not defined.
#
# The script will print the a colon-separated-value entry of the user record
#  created for data keep. The entry will be in the format of,
#  <username>:<uid><gid>:<fullname>:<clear-text password>:<encrypted password>
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common.sh;
source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common_admin.sh;

if [ $? -ne 0 ]; then
    # FAILED TO SOURCE COMMON_ADMIN.SH FOR ACQUIRING NFS CREDENTIALS
    #
    echo "FAIL: Inadequate User Permission to Proceed";

    exit 1;
fi;

usage="Create New User Account on the LDAP Server\n
Usage: $0 <username> <groupname> <lastname> [-f <firstname>] [-m <email>]\n
 e.g.: $0 abaker staff Baker -f Adams\\ Davis -m adams.baker@hku.hk\n
  or   $0 elec2401_20162A ug \"Computer Architecture Spring 2016\" -m elec2401@eee.hku.hk";

# PARSE COMPULSORY PARAMETERS
#
if [ $# -ge 3 ]; then
    usrid=$1;
    grpid=$2;
    sname=$3;
else echo -e $usage;

    exit 1;
fi;

shift 3;

# PARSE OPTIONAL PARAMETERS
#
while getopts "f:m:" opt; do
    case ${opt} in
        m) email=${OPTARG} ;;
        f) gname=${OPTARG} ;;
        *) echo -e $usage; exit 1 ;;
    esac;
done;

if [ -z ${ldappw+x} ]; then
    echo -n "LDAP Administrator password: "
    read -s ldappw;
    echo "";
fi;

clrpw=$(printf "%04X%04X" $RANDOM $RANDOM);

# slap_exist=$(which slappasswd 2> /dev/null > /dev/null; echo $?);
#
# if [ $slap_exist -ne 0 ]; then
#     encpw=$(ssh $(whoami)@$slaputilsrv slappasswd -h "{SSHA}" -s $clrpw);
#
#     if [ $? -ne 0 ]; then
#         echo "FAILED: slappasswd Not Found. Password Generation Failed";
#
#         exit 1;
#     fi;
# else
#     encpw=$(slappasswd -h "{SSHA}" -s $clrpw);
# fi;
#
# if [ $? -ne 0 ]; then
#     echo "FAILED: Password Generation Failed";
#
#     exit 1;
# fi;

if [ ! -z "$gname" ]; then
    fname="$gname $sname";
else
    fname=$sname;
fi;

#
# STAGE 2
# #######
#
# LDAP DATA QUERY AND PRE-CONDITION CHECK
#

# CHECK FOR REDUNDANCY.
#
if [ `ldapsearch -x -LLL -H $ldapsrv_prot://$ldapsrv -b "ou=$user_ou,$base_dn" "(uid=$usrid)" 1.1 | wc -l` -ne 0 ]; then
    echo "FAILED: User $usrid Already Exist";

    exit 1;
fi;

# FIND MAXIMUM UID DEFINED
#
maxuid=0;

for i in `ldapsearch -x -LLL -H $ldapsrv_prot://$ldapsrv -b "ou=$user_ou,$base_dn" 1.3.6.1.1.1.1.0 | grep "uidNumber" | cut -d ' ' -f2`; do if [ $i -gt $maxuid ]; then maxuid=$i; fi; done;

# FIND GID OF THE GROUP USER TARGETED TO ASSOCIATE WITH
#
gid=`ldapsearch -x -LLL -H $ldapsrv_prot://$ldapsrv -b "ou=$group_ou,$base_dn" "(&(objectClass=posixGroup)(cn=$grpid))" 1.3.6.1.1.1.1.1 | grep "gidNumber" | cut -d " " -f2`

if [ -z "$gid" ]; then
    echo "FAILED: Group $grpid Not Defined";

    exit 1;
fi;

#
# STAGE 3
# #######
#
# LDAP ENTRY CONSTRUCTION AND COMMIT
#

# CONSTRUCT LDAP DATA INTERCHANGE FORMAT ENTRY
#
ldif="dn: uid=$usrid,ou=$user_ou,$base_dn
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
cn: $fname
uid: $usrid
sn: $sname";

if [ ! -z "$gname" ]; then
    ldif="$ldif
givenName: $gname";
fi;

ldif="$ldif
displayName: $fname
uidNumber: $((maxuid+1))
gidNumber: $gid";
# userPassword: $encpw";

if [ ! -z "$email" ]; then
    ldif="$ldif
mail: $email";
fi;

ldif="$ldif
homeDirectory: /home/$usrid
loginShell: /bin/bash";

ldapadd -H $ldapsrv_prot://$ldapsrv -D "cn=$root_cn,$base_dn" -w $ldappw <<-!
$ldif
!

if [ $? -ne 0 ]; then
    echo "FAILED: User Creation on LDAP Server Failed";

    exit 1;
fi;

ldappasswd -H $ldapsrv_prot://$ldapsrv -D "cn=$root_cn,$base_dn" -w $ldappw -S "uid=$usrid,ou=$user_ou,$base_dn" -s $clrpw

if [ $? -ne 0 ]; then
    echo "FAILED: Set Initial Password";

    exit 1;
fi;

# ssh $nfs_root@$nfssrv "mkdir -m 700 $nfs_path/$usrid && chown $uid:$gid $nfs_path/$usrid";
idfile=`mktemp`;

if [ $? -ne 0 ]; then
    echo "FAILED: Create Identity File";

    exit 1;
fi;

cat > $idfile <<!
$nfs_privkey
!
# ADMIN@BEANBAG

# SSH CONSUMES STDIN AND BREAK OUTER WHILE LOOP READING BATCH FILE
# (site: http://stackoverflow.com/a/9393147)
#
# ssh -i $idfile $nfs_root@$nfssrv "mkdir -m 700 $nfs_path/$usrid && chown $((maxuid+1)):$gid $nfs_path/$usrid";
ssh -i $idfile $nfs_root@$nfssrv "mkdir -m 700 $nfs_path/$usrid && chown $((maxuid+1)):$gid $nfs_path/$usrid" < /dev/null;

if [ $? -ne 0 ]; then
    echo "FAILED: Home Directory Creation Failed";

    rm -f $idfile;

    exit 1;
fi;

rm -f $idfile;

echo "SUCCESS: Created user $usrid";
echo "ENTRY: $usrid:$((maxuid+1)):$gid ($grpid):$fname:$clrpw";
