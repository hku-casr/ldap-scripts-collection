#!/bin/bash

#
# Name: common_host.sh
#
# Description: This script defines host dependent variables for scripts under
#  the LDAP sysadmin category.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

if [ -z "$host" ] && [ -z "$clus" ]; then
    echo "FAIL: Host and Cluster Undefined";

    exit 1;
fi;

auth_proj="/etc/ssh";

if [ "$host" == "bluemountain" ]; then
    key_repo="/etc/ssh/authorized_keys";
fi;
