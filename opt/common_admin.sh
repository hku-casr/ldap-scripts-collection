#!/bin/bash

#
# Name: common_admin.sh
#
# Description: This script defines restricted variables for scripts under the
#  LDAP sysadmin category.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

nfssrv="beanbag";
nfs_path="/share/nfs_home";
nfs_root="admin";
nfs_privkey="-----BEGIN RSA PRIVATE KEY-----
...
-----END RSA PRIVATE KEY-----";
