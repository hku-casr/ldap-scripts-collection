#!/bin/bash

#
# Name: sshkey_add.sh
#
# Description: This script Adds a SSH Public Key to the caller LDAP entry. The
#  key may be used for authentication if the system supports SSH Public Key
#  authentication against a LDAP server.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common.sh;

if [ $# -eq 2 ]; then
    usrid=$1;
    keyf=$2;
elif [ $# -eq 1 ]; then
    usrid=`whoami`;
    keyf=$1;
else
    echo "Add SSH Public Key to the LDAP entry of the calling user";
    echo "Usage: $0 [<username>] <pub_key_file>";
    echo " e.g.: $0 .ssh/id_rsa.pub";
    echo "  or   $0 abaker .ssh/id_rsa.pub"
    exit 1;
fi;

echo "Adding ssh public key for $usrid";

if [ $usrid == `whoami` ]; then
    echo -n "UNIX password: ";
else
    echo -n "LDAP Administrator password: ";
fi;

read -s passwd;
echo -e "";

errval=1;

while read line; do
    ssh-keygen -B -f /dev/stdin <<< $line > /dev/null;
    if [ $? -eq 0 ]; then
        errval=0; # CONTAINS AT LEAST 1 VALID KEY

        ldif="dn: uid=$usrid,ou=$user_ou,$base_dn
changetype: modify
add: sshPublicKey
sshPublicKey: $line";

        if [ $usrid == `whoami` ]; then
            ldapmodify -H $ldapsrv_prot://$ldapsrv -D "uid=$usrid,ou=$user_ou,$base_dn" -w $passwd <<!
$ldif
!
        else
            ldapmodify -H $ldapsrv_prot://$ldapsrv -D "cn=$root_cn,$base_dn" -w $passwd <<!
$ldif
!
        fi;
    fi;
done < <(cat $keyf);

if [ $errval -eq 1 ]; then
    echo "FAILED: File contains no valid SSH Public Key";

    exit 1;
fi;
