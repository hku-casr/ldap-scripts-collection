#!/bin/bash

#
# Name: ldap_batch.sh
#
# Description: This script creates new user accounts or modify user accounts
#  attributes in batch mode. The script parses a file of colon-separated-value
#  entries containing either user account attributes for creating new posix
#  account or user-action pairs that specify actions to be conducted upon an
#  existing user account.
#
# For user account creation, the entries shall be,
#  <username>:<groupname>:<lastname>[:[firstname]:email]
#
# For user account attributes modification, the entries shall be,
#  <username>:{add | modify | delete}:<attribute>[:<predicate>[:new_value]]
#  Action "add" shall be performed with new_value specified.
#  Action "modify" shall be performed with predicate and new_value specified.
#  Action "delete" may go with predicate when attribute is not singular.
#
# Optional arguments might be omitted when appropriate and their preceding
#  consecutively replicating colons might be omitted altogether. Arguments
#  might also be omitted by providing nothing within the field.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#

source `dirname $(readlink -f ${BASH_SOURCE[0]})`/common.sh;

usage="Create New or Modify User Accounts on the LDAP Server in Batch Mode.\n
This script parses entries in file of colon delimited format with positional\n
-sensitivity, the positional parameter for adding new users shall be,\n
Entry: <username>:<groupname>:<lastname>[:[firstname]:email]\n
 e.g.: abaker:staff:Baker:Adams Davis:adams.baker@hku.hk\n
  or   elec2401_20162A:ug:Computer Architecture Spring 2016::elec2401@eee.hku.hk\n
  or   elec2401_20152A:ug:Computer Architecture Spring 2015\n
As for modifying current users attributes, the positional parameters shall be,\n
Entry: <username>:{add | modify | delete}:<attribute>[:<predicate>[:new_value]]\n
 e.g.: abaker:add:host::americano00\n
  or   abaker:delete:host:bluemountain\n
  or   abaker:modify:host:americano00:americano01\n
Usage: $0 {add | modify} <filename>";

# PARSE COMPULSORY PARAMETERS
#
if [ $# -eq 2 ]; then
    filename=$2;
else echo -e $usage;
    exit 1;
fi;

proj=`dirname $(readlink -f ${BASH_SOURCE[0]})`;

echo -n "LDAP Administrator password: "
read -s ldappw;
echo "";

if [ $1 == "add" ]; then
    while read line; do
        IFS=":" read username groupname lastname firstname email <<< "$line";

        opt=""

        if [ ! -z "$firstname" ] && [ ! -z "$email" ]; then
            ldappw=$ldappw $proj/ldap_adduser.sh "$username" "$groupname" "$lastname" -f "$firstname" -m "$email";
        elif [ ! -z "$firstname" ]; then
            ldappw=$ldappw $proj/ldap_adduser.sh "$username" "$groupname" "$lastname" -f "$firstname";
        elif [ ! -z "$email" ]; then
            ldappw=$ldappw $proj/ldap_adduser.sh "$username" "$groupname" "$lastname" -m "$email";
        else
            ldappw=$ldappw $proj/ldap_adduser.sh "$username" "$groupname" "$lastname";
        fi;
    done < $filename;
elif [ $1 == "modify" ]; then
    while read line; do
        IFS=":" read username action attribute predicate new_val <<< "$line";

        if [ -z "$username" ] || [ -z "$attribute" ]; then
            echo -e $usage;
        fi;

        case "$action" in
            add) if [ -z "$new_val" ] || [ ! -z "$predicate" ]; then echo -e $usage; exit 1; fi; ;;
            modify) if [ -z "$new_val" ]; then echo -e $usage; exit 1; fi; ;;
            delete) if [ ! -z "$new_val" ]; then echo -e $usage; exit 1; fi; ;;
            *) echo -e $usage; exit 1; ;;
        esac;

        if [ ! -z "$predicate" ] && [ ! -z "$new_val" ]; then
            ldappw=$ldappw $proj/ldap_usermod.sh "$username" "$action" "$attribute" "$predicate" "$new_val";
        elif [ ! -z "$predicate" ]; then
            ldappw=$ldappw $proj/ldap_usermod.sh "$username" "$action" "$attribute" "$predicate";
        elif [ ! -z "$new_val" ]; then
            ldappw=$ldappw $proj/ldap_usermod.sh "$username" "$action" "$attribute" "$new_val";
        else
            ldappw=$ldappw $proj/ldap_usermod.sh "$username" "$action" "$attribute";
        fi;
    done < $filename;
fi;
